/*
  React js - syntax used is JSX.

  JSX - javascript + XML, it is an extension of JS that lets us create objects which will be compiled as HTML Elements.

  With JSX, we are able to create JS Object with HTML like syntax
  
  ReactDOM.render(<reactElement>, <htmlELementSelectedById>)

  ReactJS - doesn't like returning 2 elements unless they are wrapped by another element, or what we call a fragment(<></>)

*/



import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

/*let element = <h1>My First React App!</h1>
//console.log(element)
with JSX:
let myName = <h2>Louise Vivien Simpelo</h2>
without JSX
React.createElement(<tag>,<data>, <content>)
let myName = React.createElement('h2', {}, "This was not created with JSX Syntax.")
*/

let person = {

  name: "Stephen Strange",
  age: 45,
  job: "Sorcerer Supreme",
  income: 50000,
  expense: 30000
}

let sorcererSupreme1 = <p>My name is {person.name}. I am {person.age} years old. I work as {person.job}</p>

let sorcererSupreme2 = <p>I am {person.name}. I work as a {person.job}. My income is {person.income}. My expense is {person.expense}. My balance is {person.income - person.expense}</p>

let sorcererSupreme =(
    <div>
      <p>My name is {person.name}. I am {person.age} years old. I work as {person.job}</p>
      <p>I am {person.name}. I work as a {person.job}. My income is {person.income}. My expense is {person.expense}. My balance is {person.income - person.expense}</p>
    </div>
  ) 

ReactDOM.render(sorcererSupreme, document.getElementById('root'));